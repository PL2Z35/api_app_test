FROM openjdk:11
RUN mkdir /opt/app
COPY target/spring-boot-api-jar-with-dependencies.jar opt/app/spring-boot-api-jar-with-dependencies.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","opt/app/spring-boot-api-jar-with-dependencies.jar"]
CMD ["-start"]

