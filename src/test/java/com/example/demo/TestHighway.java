package com.example.demo;

import com.example.demo.dto.HighwayDTO;
import com.example.demo.dto.HistoryDTO;
import com.example.demo.dto.TrafficPoliceDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@Repository
public class TestHighway {
    private static final String FOO_RESOURCE_URL = "http://localhost:3000";

    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * Método que testea la obtención una via.
     * <p>
     * 1. Revisa el status de la respuesta.
     * 2. Comprueba que el dato de la via sea igual al almacenado en la base de datos.
     */
    @Test
    @Sql(statements = "INSERT INTO highway VALUES (99,1.0,1,'calle','carretera')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM highway WHERE id=99", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testGetHighway() {
        ResponseEntity response = WebClient.create(FOO_RESOURCE_URL).get().uri("/highway/99").retrieve().onStatus(status -> status == HttpStatus.OK, clientResponse -> Mono.empty()).toEntity(String.class).block();
        assertEquals(response.getStatusCode(), (HttpStatus.OK));
        assertThat(highwayDTOTest().equals(response.getBody()));
    }

    /**
     * Método que testea la obtención las todas las vias.
     * <p>
     * 1. Almacena una via antes de la ejecución de test.
     * 2. Evalúa que la respuesta sea mayor a cero.
     */
    @Test
    @Sql(statements = "INSERT INTO highway VALUES (99,1.0,1,'calle','carretera')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM highway WHERE id=99", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testGetAllHighway() {
        Long resp = WebClient.create(FOO_RESOURCE_URL).get().uri("/highway/all").retrieve().bodyToFlux(TrafficPoliceDTO.class).count().block();
        assertTrue(resp > 0);
    }

    /**
     * Método que testea la inserción de una nueva via.
     * <p>
     * 1. Compara que el status recibido sea el correcto.
     * 2. Compara el objeto enviado con el recibido para que sean iguales.
     * 3. Verifica que la tabla de objetos solo aumente en 1 dato.
     * 4. Revisa que en la lista únicamente exista un objeto con el mismo identificador.
     */
    @Test
    @Sql(statements = "DELETE FROM highway WHERE id=99", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testPostHighway() {
        int countBefore = count();
        ResponseEntity response = WebClient.create(FOO_RESOURCE_URL).post().uri("/highway").body(Mono.just(highwayDTOTest()), HistoryDTO.class).retrieve().onStatus(status -> status == HttpStatus.OK, clientResponse -> Mono.empty()).toEntity(String.class).block();
        assertEquals(response.getStatusCode(), (HttpStatus.CREATED));
        assertThat(highwayDTOTest().equals(response.getBody()));
        assertEquals(countBefore + 1, findAll().size());
        assertEquals(1, findAll().stream().filter(highwayDTO -> highwayDTO.getId() == highwayDTOTest().getId()).count());
    }

    /**
     * Método que testea la modificación de un objeto tipo via.
     * <p>
     * 1. Guarda un objeto tipo via.
     * 2. Realiza un cambio el atributo StreetOrRace y lo envió por el método.
     * 3. Revisa que el status sea correcto.
     * 4. Verifica que el dato enviado y el obtenido sean iguales.
     * 5. Busca en la tabla de vias que solo exista una con este identificador.
     */
    @Test
    @Sql(statements = "INSERT INTO highway VALUES (99,1.0,1,'calle','carretera')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM highway WHERE id=99", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testPutHighway() {
        HighwayDTO highwayDTOAux = highwayDTOTest();
        highwayDTOAux.setStreetOrRace("aa");
        ResponseEntity resp = WebClient.create(FOO_RESOURCE_URL).put().uri("/highway").body(Mono.just(highwayDTOTest()), HighwayDTO.class).retrieve().onStatus(status -> status == HttpStatus.OK, clientResponse -> Mono.empty()).toEntity(String.class).block();
        assertEquals(resp.getStatusCode(), HttpStatus.OK);
        assertEquals(1, findAll().stream().filter(highwayDTO -> highwayDTO.getId() == highwayDTOTest().getId()).count());
    }

    /**
     * Método para realizar el test de la eliminación de una via.
     * <p>
     * 1. Comprueba que se almacena una via antes del test.
     * 2. Revisa que la respuesta sea correcta.
     * 3. Verifica que ya no exista el agente de tránsito.
     */
    @Test
    @Sql(statements = "INSERT INTO highway VALUES (99,1.0,1,'calle','carretera')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM highway WHERE id=99", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testDeleteHighway() {
        assertEquals(1, findAll().stream().filter(highwayDTO -> highwayDTO.getId() == highwayDTOTest().getId()).count());
        ResponseEntity resp = WebClient.create(FOO_RESOURCE_URL).delete().uri("/highway/" + highwayDTOTest().getId()).retrieve().onStatus(status -> status == HttpStatus.OK, clientResponse -> Mono.empty()).toEntity(String.class).block();
        assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertEquals(0, findAll().stream().filter(highwayDTO -> highwayDTO.getId() == highwayDTOTest().getId()).count());
    }

    /**
     * Método que almacena el objeto de tipo via para las pruebas.
     *
     * @return HighwayDTO: objeto que se utilizan para testear las rutas.
     */
    public static HighwayDTO highwayDTOTest() {
        HighwayDTO highwayDTO = new HighwayDTO();
        highwayDTO.setId(99);
        highwayDTO.setCongestionLevel(1.0);
        highwayDTO.setNumber(1);
        highwayDTO.setStreetOrRace("calle");
        highwayDTO.setType("carretera");
        return highwayDTO;
    }

    /**
     * Método para traer todos los datos de la tabla highway
     *
     * @return List<HighwayDTO>: todos los datos de la tabla.
     */
    private List<HighwayDTO> findAll() {
        String sql = "SELECT * FROM highway";
        return jdbcTemplate.query(sql, ((rs, rowNum) -> new HighwayDTO(rs.getInt("id"), rs.getString("type"), rs.getString("street_or_race"), rs.getInt("number"), rs.getDouble("congestion_level"))));
    }
    /**
     * Método para saber la cantidad de datos de la tabla highway.
     *
     * @return int: cantidad de vias registradas.
     */
    private int count() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM highway", Integer.class);
    }
}
