package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.reactive.function.client.WebClient;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.dto.*;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Repository
class TestTrafficPolice {

    private static final String FOO_RESOURCE_URL = "http://localhost:3000";

    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * Método para realizar el test de la eliminación de un nuevo agente de tránsito.
     * <p>
     * 1. Comprueba que el agente de tránsito existe.
     * 2. Revisa que la respuesta sea correcta.
     * 3. Verifica que ya no exista el agente de tránsito.
     */
    @Test
    @Sql(statements = "INSERT INTO highway VALUES (99,1.0,1,'calle','carretera')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "INSERT INTO traffic_police VALUES (23,'test','test', 1.0, 99)", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM traffic_police WHERE id=23", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @Sql(statements = "DELETE FROM highway WHERE id=99", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testDeleteTrafficPolice() {
        assertEquals(1, findAll().stream().filter(trafficPoliceDTO -> trafficPoliceDTO.getId().equals(trafficPoliceTest().getId())).count());
        ResponseEntity resp = WebClient.create(FOO_RESOURCE_URL).delete().uri("/trafficPolice/" + trafficPoliceTest().getId()).retrieve().onStatus(status -> status == HttpStatus.OK, clientResponse -> Mono.empty()).toEntity(String.class).block();
        assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertEquals(0, findAll().stream().filter(trafficPoliceDTO -> trafficPoliceDTO.getId().equals(trafficPoliceTest().getId())).count());
    }

    /**
     * Método para realizar el test de una inserción de un nuevo agente de tránsito.
     * <p>
     * 1. Hace una comparación del objeto recibido y el enviado por el método Post.
     * 2. Revisa que la cantidad de datos en la tabla traffic_police solo aumentara en un dato.
     * 3. Extrae los datos de la tabla y verifica que el objeto únicamente se encuentre una vez registrado.
     * 4. Se envió de nuevo los datos para validar que no se puedan agregar dos datos iguales y que se recibe un estatus 409.
     */
    @Test
    @Sql(statements = "INSERT INTO highway VALUES (99,1.0,1,'calle','carretera')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM traffic_police WHERE id=23", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @Sql(statements = "DELETE FROM highway WHERE id=99", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testPostTrafficPolice() {
        int countBefore = count();
        ResponseEntity response = WebClient.create(FOO_RESOURCE_URL).post().uri("/trafficPolice").body(Mono.just(trafficPoliceTest()), TrafficPoliceDTO.class).retrieve().onStatus(status -> status == HttpStatus.OK, clientResponse -> Mono.empty()).toEntity(String.class).block();
        assertEquals(response.getStatusCode(), (HttpStatus.CREATED));
        assertThat(trafficPoliceTest().equals(response.getBody()));
        assertEquals(countBefore + 1, findAll().size());
        assertEquals(1, findAll().stream().filter(trafficPoliceDTO -> trafficPoliceDTO.getId().equals(trafficPoliceTest().getId())).count());
    }

    /**
     * Método que testea la eliminación de un agente de tránsito.
     * <p>
     * 1. Compara que el objeto extraído de la base de datos sea igual al que tiene el mismo identificador.
     * 2. Revisa que este objeto solamente se repita una vez en la base de datos.
     */
    @Test
    @Sql(statements = "INSERT INTO highway VALUES (99,1.0,1,'calle','carretera')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "INSERT INTO traffic_police VALUES (23,'test','test', 1.0, 99)", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM traffic_police WHERE id=23", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @Sql(statements = "DELETE FROM highway WHERE id=99", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testGetTrafficPolice() {
        ResponseEntity resp = WebClient.create(FOO_RESOURCE_URL).get().uri("/trafficPolice/" + trafficPoliceTest().getId()).retrieve().onStatus(status -> status == HttpStatus.OK, clientResponse -> Mono.empty()).toEntity(String.class).block();
        assertThat(resp.equals(trafficPoliceTest()));
        assertThat(resp.getStatusCode().equals(HttpStatus.OK));
        assertEquals(1, findAll().stream().filter(trafficPoliceDTO -> trafficPoliceDTO.getId().equals(trafficPoliceTest().getId())).count());
    }

    /**
     * Método que testea la obtención de la lista de los agentes de tránsito.
     * <p>
     * 1. Revisa que la lista bno este vacía.
     * 2. Comprueba que exista solamente un valor con el dato insertado desde el archivo sql.
     */
    @Test
    @Sql(statements = "INSERT INTO highway VALUES (99,1.0,1,'calle','carretera')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "INSERT INTO traffic_police VALUES (23,'test','test', 1.0, 99)", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM traffic_police WHERE id=23", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @Sql(statements = "DELETE FROM highway WHERE id=99", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testGetAllTrafficPolice() {
        Long resp = WebClient.create(FOO_RESOURCE_URL).get().uri("/trafficPolice/all").retrieve().bodyToFlux(TrafficPoliceDTO.class).count().block();
        assertTrue(resp > 0);
        assertEquals(1, findAll().stream().filter(trafficPoliceDTO -> trafficPoliceDTO.getId().equals(trafficPoliceTest().getId())).count());
    }

    /**
     * Método que testea la modificación de la información de un agnete de tránsito.
     * <p>
     * 1. Compara el que el nombre que se cambió al agente de tránsito sea igual al que se retorna con esta petición.
     */
    @Test
    @Sql(statements = "INSERT INTO highway VALUES (99,1.0,1,'calle','carretera')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "INSERT INTO traffic_police VALUES (23,'test','test', 1.0, 99)", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM traffic_police WHERE id=23", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @Sql(statements = "DELETE FROM highway WHERE id=99", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testPutTrafficPolice() {
        TrafficPoliceDTO trafficPoliceDTOAux = trafficPoliceTest();
        trafficPoliceDTOAux.setName("TestPut");
        ResponseEntity resp = WebClient.create(FOO_RESOURCE_URL).put().uri("/trafficPolice").body(Mono.just(trafficPoliceDTOAux), TrafficPoliceDTO.class).retrieve().onStatus(status -> status == HttpStatus.OK, clientResponse -> Mono.empty()).toEntity(String.class).block();
        assertEquals(resp.getStatusCode(), HttpStatus.OK);
        assertEquals(1, findAll().stream().filter(trafficPoliceDTO -> trafficPoliceDTO.getId().equals(trafficPoliceTest().getId())).count());
    }

    /**
     * Método que almacena el objeto de tipo agente de tránsito para las pruebas.
     *
     * @return TrafficPoliceDTO: objeto que se utilizan para testear las rutas.
     */
    public static TrafficPoliceDTO trafficPoliceTest() {
        TrafficPoliceDTO trafficPoliceDTO = new TrafficPoliceDTO();
        trafficPoliceDTO.setId((long) 23);
        trafficPoliceDTO.setName("test");
        trafficPoliceDTO.setIdSecretary("test");
        trafficPoliceDTO.setYearsExperience(1.0);
        trafficPoliceDTO.setHighway(TestHighway.highwayDTOTest().getId());
        return trafficPoliceDTO;
    }

    /**
     * Método para saber la cantidad de datos de la tabla traffic_police.
     *
     * @return int: cantidad de agentes de tránsito registrados.
     */
    private int count() {
        return jdbcTemplate.queryForObject("select count(*) from traffic_police", Integer.class);
    }

    /**
     * Método para traer todos los datos de la tabla traffic_police.
     *
     * @return List<TrafficPoliceDTO>: todos los datos de la tabla.
     */
    private List<TrafficPoliceDTO> findAll() {
        String sql = "SELECT * FROM traffic_police";
        return jdbcTemplate.query(sql, ((rs, rowNum) -> new TrafficPoliceDTO(rs.getLong("id"), rs.getString("name"), rs.getDouble("years_experience"), rs.getString("id_secretary"), rs.getInt("highway"))));
    }

}
