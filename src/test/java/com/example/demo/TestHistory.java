package com.example.demo;

import com.example.demo.dto.HistoryDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Repository
public class TestHistory {

    private static final String FOO_RESOURCE_URL = "http://localhost:3000";
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * Método para extraer las historias creadas de cada via.
     * <p>
     * Envía una petición para obtener las historias de las rutas.
     * Revisa que el estatus traído sea Ok.
     *
     * @throws JsonProcessingException
     */
    @Test
    void testGetHistoryHighway() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ResponseEntity resp = WebClient.create(FOO_RESOURCE_URL).get().uri("/highway/2/history").retrieve().onStatus(status -> status == HttpStatus.OK, clientResponse -> Mono.empty()).toEntity(String.class).block();
        assertEquals(resp.getStatusCode(), HttpStatus.OK);
    }

    /**
     * Método para extraer las historias creadas de cada agente de tránsito.
     * <p>
     * Envía una petición para obtener las historias de las rutas.
     * Revisa que el estatus traído sea Ok.
     *
     * @throws JsonProcessingException
     */
    @Test
    void testGetHistoryTrafficPolice() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ResponseEntity resp = WebClient.create(FOO_RESOURCE_URL).get().uri("/trafficPolice/" + TestTrafficPolice.trafficPoliceTest().getId() + "/history").retrieve().onStatus(status -> status == HttpStatus.OK, clientResponse -> Mono.empty()).toEntity(String.class).block();
        assertEquals(resp.getStatusCode(), HttpStatus.OK);
    }

    /**
     * Método para traer todos los datos de la tabla history.
     *
     * @return List<TrafficPoliceDTO>: todos los datos de la tabla.
     */
    private List<HistoryDTO> findAll() {
        String sql = "SELECT * FROM history";
        return jdbcTemplate.query(sql, ((rs, rowNum) -> new HistoryDTO(rs.getInt("id"), rs.getLong("transit_agent_id"), rs.getInt("highway_id"), rs.getString("date"))));
    }
}
