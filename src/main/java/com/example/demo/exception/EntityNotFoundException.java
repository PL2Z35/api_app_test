package com.example.demo.exception;

/**
 * @author: Cristian Plazas
 * @since 6/4/2022
 * @version: 1.0
 */

/**
 * Clase para manejo de excepciones de la API.
 */
public class EntityNotFoundException extends Exception{

    /**
     * Constructor de la clase.
     *
     * Ingresa un mensaje de error.
     */
    public EntityNotFoundException(String message){
        super(message);
    }
}
