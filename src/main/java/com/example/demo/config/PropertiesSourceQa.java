package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * Clase que configura las propiedades del perfil de pruebas para la base de datos.
 */
@Configuration
@PropertySource("classpath:application-qa.properties")
@Profile("qa")
public class PropertiesSourceQa {

}
