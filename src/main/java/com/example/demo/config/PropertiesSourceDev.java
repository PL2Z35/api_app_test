package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;


/**
 * Clase que configura las propiedades del perfil de desarrollo para la base de datos.
 */
@Configuration
@PropertySource("classpath:application-dev.properties")
@Profile("dev")
public class PropertiesSourceDev {

}
