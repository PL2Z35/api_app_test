package com.example.demo.service.impl;

import java.util.List;
import java.util.ArrayList;

import com.example.demo.entity.Highway;

import com.example.demo.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.repository.HighwayRepository;
import com.example.demo.service.*;

/**
 * Implementacion de los servicios para el objeto via.
 *
 * @author : Cristian Plazas
 * @since : 6/27/2022
 */

@Service
public class HighwayServiceImpl implements HighwayService {

    @Autowired
    private HighwayRepository repository;

    /**
     * Método que obtiene una lista de todas las calles.
     *
     * @return Lista de calles.
     */
    @Override
    public List<Highway> allList() {
        return repository.findAll();
    }

    /**
     * Método que obtiene una lista las vias disponibles para asignarlas.
     *
     * @return Lista de calles.
     */
    @Override
    public List<Highway> allAvailable() {
        return new ArrayList<>();
    }

    /**
     * Método que guarda una nueva via.
     *
     * @param highway: Objeto de tipo via.
     * @return Highway: si el objeto no existe en la base de datos o null: si el objeto existe en la base de datos.
     */
    @Override
    public Highway saveHighway(Highway highway) {
        return repository.existsById(highway.getId()) || highway.getId() == 0 ? null : repository.save(highway);
    }

    /**
     * Método que obtiene una via por su identificador.
     *
     * @param id: id Identificador de la via.
     * @return Highway: Objeto existe en la base de datos.
     * @throws EntityNotFoundException : Si no existe la via.
     */
    @Override
    public Highway getHighwayId(int id) {
        return repository.getReferenceById(id);
    }

    /**
     * Método que actualiza los datos de una via.
     *
     * @param highway Objeto de tipo via.
     * @return Highway: si el objeto existe en la base de datos o null: si el objeto no existe en la base de datos.
     */
    @Override
    public Highway updateHighway(Highway highway) {
        return repository.existsById(highway.getId()) ? repository.save(highway) : null;
    }

    /**
     * Método que elimina una via.
     * <p>
     * Este método busca una via por su identificador y la elimina.
     *
     * @param id: Identificador de la via.
     * @throws NullPointerException: Si no existe la via.
     */

    @Override
    public void deleteHighway(int id) throws EntityNotFoundException {
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            throw new EntityNotFoundException("No existe la via");
        }
    }
}