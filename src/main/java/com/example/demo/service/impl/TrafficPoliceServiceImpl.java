package com.example.demo.service.impl;

import java.util.List;

import com.example.demo.entity.History;
import com.example.demo.entity.TrafficPolice;

import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import com.example.demo.repository.TrafficPoliceRepository;
import com.example.demo.service.*;

/**
 * Implementacion de los servicios para el objeto agente de tránsito.
 *
 * @author : Cristian Plazas
 * @since : 6/27/2022
 */
@PropertySource(ignoreResourceNotFound = true, value = "classpath:application.properties")
@Service
public class TrafficPoliceServiceImpl implements TrafficPoliceService {

    @Autowired
    private TrafficPoliceRepository trafficPoliceRepository;

    @Autowired
    private HistoryRepository historyRepository;

    @Value("${max.level.congestion}")
    public int maxCongestionLevel;

    @Value("${max.assignments.highway}")
    public int maxAssignmentHighway;

    /**
     * Método que obtiene una lista de todos los agentes de tránsito.
     *
     * @return List: Lista de objetos del agente de tránsito.
     */
    @Override
    public List<TrafficPolice> allList() {
        return trafficPoliceRepository.findAll();
    }

    /**
     * Método que guarda un nuevo agente de tránsito.
     * <p>
     * Guarda los datos del nuevo objeto en la base de datos.
     * Genera una nueva historia.
     *
     * @return TrafficPolice: Si el agente de transito no existe en la base de
     * datos.
     * @return null: Si el agente de tránsito ya existe en la base de datos.
     * @return null: Si la via que se desea modificar ya fue asignada por el dato max.assignments.highway o si
     * * la via tiene un nivel de congestion mayor al dato max.level.congestion.
     * @params TrafficPolice: Objeto agente de transito.
     */
    @Override
    public TrafficPolice saveTransitAgent(TrafficPolice trafficPolice) {
        if (trafficPolice.getHighway().getCongestionLevel()<=maxCongestionLevel && getContAssignmentsHighway(trafficPolice, trafficPolice.getHighway().getId())<maxAssignmentHighway && !trafficPoliceRepository.existsById(trafficPolice.getId())){
            History history = new History();
            history.setTransitAgentId(trafficPolice.getId());
            history.setDate("-");
            history.setHighwayId(trafficPolice.getHighway().getId());
            historyRepository.save(history);
            return trafficPoliceRepository.save(trafficPolice);
        }else{
            return null;
        }
    }

    /**
     * Método que obtiene un agente de tránsito por su identificador.
     *
     * @return TrafficPolice : Si existe un agente de transito con el identificador.
     * @throws NullPointerException: Si no existe un agente de tránsito con el identificador.
     * @params long: Identificador del agente de tránsito.
     */
    @Override
    public TrafficPolice getTransitAgentId(Long id) throws NullPointerException {
        try {
            return trafficPoliceRepository.getReferenceById(id);
        } catch (Exception e) {
            throw new NullPointerException("");
        }
    }

    /**
     * Método que actualiza un agente de tránsito.
     * <p>
     * Actualiza cada parámetro del agente de tránsito.
     * Exceptuando el identificador.
     * Crea una nueva historia.
     *
     * @return TrafficPolice : Si existe el agente de transito.
     * @return null: Si la via que se desea modificar ya fue asignada por el dato max.assignments.highway o si
     * la via tiene un nivel de congestion mayor al dato max.level.congestion.
     * @return null: si no existe el agente de transito.
     * @params TrafficPolice : Objeto tipo agente de transito.
     */
    @Override
    public TrafficPolice updateTransitAgent(TrafficPolice trafficPolice) {
        if (trafficPolice.getHighway().getCongestionLevel()<=maxCongestionLevel && getContAssignmentsHighway(trafficPolice, trafficPolice.getHighway().getId())<maxAssignmentHighway && trafficPoliceRepository.existsById(trafficPolice.getId())){
            History history = new History();
            history.setTransitAgentId(trafficPolice.getId());
            history.setDate("-");
            history.setHighwayId(trafficPolice.getHighway().getId());
            historyRepository.save(history);
            return trafficPoliceRepository.save(trafficPolice);
        }else{
            return null;
        }
    }

    /**
     * Método que elimina un agente de tránsito.
     * <p>
     * Busca el agente de tránsito por su identificador y lo elimina.
     *
     * @throws NullPointerException: Si no existe un agente de tránsito con el identificador.
     * @params long: Identificador del agente de tránsito.
     */
    @Override
    public void deleteTransitAgent(Long id) throws EntityNotFoundException {
        try {
            trafficPoliceRepository.deleteById(id);
        } catch (Exception e) {
            throw new EntityNotFoundException("");
        }
    }

    /**
     * Método que obtiene el mayor valor de congestion para la asignación de una
     * via.
     *
     * @return int: Valor mayor de congestion.
     */

    private int getMaxCongestionLevel() {
        return maxCongestionLevel;
    }

    /**
     * Método que obtiene el número de asignaciones de una via.
     *
     * @return int: Mayor valor de congestion para asignación.
     */
    private int getMaxAssignmentHighway() {
        return maxAssignmentHighway;
    }

    /**
     * Método que obtiene el número de asignaciones de una via.
     *
     * @return int: Número de asignaciones de la via.
     * @params integer: Identificador de la via.
     */
    private int getContAssignmentsHighway(TrafficPolice trafficPoliceAux, int id) {
        return (int) allList().stream().filter(trafficPolice -> (trafficPolice.getHighway().getId() == id && !trafficPolice.getId().equals(trafficPoliceAux.getId()))).count();
    }
}
