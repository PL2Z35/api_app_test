package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Highway;
import com.example.demo.exception.EntityNotFoundException;

/**
 * Servicios para el objeto Highway.
 *
 * @author : Cristian Plazas
 * @since : 6/27/2020
 */
public interface HighwayService {
    /**
     * Método que obtiene una lista de todas las calles.
     *
     * @return Lista de calles.
     */
    List<Highway> allList();

    /**
     * Método que obtiene una lista de todas las vias disponibles.
     *
     * @return Lista de calles.
     */
    List<Highway> allAvailable();

    /**
     * que guarda una nueva via.
     *
     * @param highway Objeto de tipo via.
     * @return Highway: si el objetivo no existe en la base de datos o null: si el objeto existe en la base de datos.
     */
    Highway saveHighway(Highway highway);

    /**
     * Método que obtiene una via por su identificador.
     *
     * @param id: id Identificador de la via.
     * @return Highway: Objeto existe en la base de datos.
     * @throws EntityNotFoundException : Si no existe la via.
     */
    Highway getHighwayId(int id);

    /**
     * Método que actualiza los datos de una via.
     *
     * @param highway Objeto de tipo via.
     * @return Highway: si el objetivo existe en la base de datos o null: si el objeto no existe en la base de datos.
     */
    Highway updateHighway(Highway highway);

    /**
     * Método que elimina una via.
     * <p>
     * Este método busca una via por su identificador y la elimina.
     *
     * @param id: Identificador de la via.
     * @throws EntityNotFoundException : Si no existe la via.
     */
    void deleteHighway(int id) throws EntityNotFoundException;
}
