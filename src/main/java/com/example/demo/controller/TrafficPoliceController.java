package com.example.demo.controller;

import com.example.demo.dto.HistoryDTO;
import com.example.demo.dto.TrafficPoliceDTO;
import com.example.demo.entity.Highway;
import com.example.demo.entity.History;
import com.example.demo.entity.TrafficPolice;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.service.HighwayService;
import com.example.demo.service.HistoryService;
import com.example.demo.service.TrafficPoliceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase controladora del objeto TrafficPolice.
 *
 * @author : Cristian Plazas
 * @since : 6/27/2020
 */
@RestController
@RequestMapping("/trafficPolice")
@CrossOrigin(origins = "http://localhost:81")
public class TrafficPoliceController {

    @Autowired
    private TrafficPoliceService serviceTransitAgent;

    @Autowired
    private HistoryService serviceHistory;

    @Autowired
    private HighwayService serviceHighway;

    /**
     * Método que devuelve todos los agentes de tránsito.
     * <p>
     * 1. Realiza una lista con todos los agentes de tránsito.
     * 2. Ejecuta una conversion de la lista del paso 1 en una lista de DTO.
     *
     * @return ResponseEntity: con estado 200 y la lista de todos los agentes de
     * tránsito.
     */
    @GetMapping("/all")
    public ResponseEntity<List<TrafficPoliceDTO>> listTrafficPolices() {
        List<TrafficPoliceDTO> listTrafficPolicesDTO = new ArrayList<>();
        List<TrafficPolice> listTrafficPolices = serviceTransitAgent.allList();
        for (TrafficPolice trafficPolice : listTrafficPolices) {
            listTrafficPolicesDTO.add(convertTrafficPoliceToDTO(trafficPolice));
        }
        return new ResponseEntity<>(listTrafficPolicesDTO, HttpStatus.OK);
    }

    /**
     * Método que devuelve un agente de tránsito por su id.
     * <p>
     * 1. Realiza una conversion de objeto de tipo TrafficPoliceDTO a objeto de tipo
     * TrafficPolice.
     * 2. Ejecuta una búsqueda del agente de tránsito por su id.
     *
     * @param id: identificador del agente de tránsito.
     * @return ResponseEntity: con estado 200 y el objeto de transferencia de datos
     * de tipo agente de tránsito.
     * @throws NullPointerException: con un estado 404, porque no se encuentra el agente de
     *                               tránsito.
     */
    @GetMapping({"/{id}"})
    public ResponseEntity<TrafficPoliceDTO> getTrafficPolice(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(convertTrafficPoliceToDTO(serviceTransitAgent.getTransitAgentId(id)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Método que lista las historias de un agente de tránsito.
     * <p>
     * 1. Realiza una lista con todos las historias del agente de tránsito.
     * 2. convierte la lista del paso 1 en una lista de DTO.
     *
     * @params Long: identificador del agente de tránsito.
     *
     * @return ResponseEntity: con estado 200 y la lista de todos las historias del
     * agente de tránsito
     *
     */
    @GetMapping({"/{id}/history"})
    public ResponseEntity<List<HistoryDTO>> getTrafficPoliceHistory(@PathVariable(value = "id") Long id) {
        List<HistoryDTO> listHistoryDTO = new ArrayList<>();
        List<History> listHistory = serviceHistory.getListTrafficPolice(id);
        for (History history : listHistory) {
            listHistoryDTO.add(convertHistoryToDTO(history));
        }
        return new ResponseEntity<>(listHistoryDTO, HttpStatus.OK);
    }

    /**
     * Método que crea un agente de tránsito.
     * <p>
     * 1. Realiza una búsqueda para ver si el identificador de la via existe.
     * 2. Ejecuta una conversion de objeto de transferencia de datos del agente de
     * tránsito a objeto tipo agente de tránsito.
     * 3. Hace una creación del agente de tránsito.
     *
     * @return ResponseEntity: con estado 200 y el objeto de transferencia de datos
     * del agente de tránsito.
     * @return ResponseEntity: con estado 400 y un objeto null si no se cumplen las
     * reglas de negocio.
     * @return ResponseEntity: con estado 400, si no se completó correctamente la
     * creación del agente de tránsito.
     * @params TrafficPoliceDTO: objeto de transferencia de datos del agente de
     * tránsito.
     */
    @PostMapping()
    public ResponseEntity<Object> saveTrafficPolice(@RequestBody TrafficPoliceDTO trafficPoliceDTO) {
        TrafficPolice trafficPolice = serviceTransitAgent.saveTransitAgent(convertDTOtoTrafficPolice(trafficPoliceDTO, serviceHighway.getHighwayId(trafficPoliceDTO.getHighway())));
        if (trafficPolice != null) {
            return new ResponseEntity<>(convertTrafficPoliceToDTO(trafficPolice), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    /**
     * Método que actualiza un agente de tránsito.
     * <p>
     * 1. Realiza una búsqueda para ver si el identificador de la via existe.
     * 2. Ejecuta una conversion de objeto de transferencia de datos del agente de
     * tránsito a objeto tipo agente de tránsito.
     * 3. Hace la creación la actualización del agente de tránsito.
     *
     * @return ResponseEntity: con estado 200 y el objeto de transferencia de datos
     * del agente de tránsito.
     * @return ResponseEntity: con estado 200 y un objeto null si no se cumplen las
     * reglas de negocio.
     * @return ResponseEntity: con estado 404, si no se completó correctamente la
     * actualización del agente de tránsito.
     * @params TrafficPoliceDTO: Objeto de transferencia de datos del agente de
     * tránsito..
     */
    @PutMapping()
    public ResponseEntity<Object> updateTrafficPolice(@RequestBody TrafficPoliceDTO trafficPoliceDTO) {
        if (serviceTransitAgent.updateTransitAgent(convertDTOtoTrafficPolice(trafficPoliceDTO, serviceHighway.getHighwayId(trafficPoliceDTO.getHighway()))) != null) {
            return new ResponseEntity<>(trafficPoliceDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Método que elimina un agente de tránsito.
     * <p>
     * 1. Envía el identificador del agente de tránsito.
     *
     * @return ResponseEntity: con estado 200 y un mensaje Delete si se borró correctamente o ResponseEntity: con estado 404 si no se encuentra el agente de tránsito.
     */
    @DeleteMapping({"/{id}"})
    public ResponseEntity<String> deleteTrafficPolice(@PathVariable(value = "id") Long id) {
        try {
            serviceTransitAgent.deleteTransitAgent(id);
            return new ResponseEntity<>("Delete", HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Método para convertir un objeto de transferencia de datos de agente de
     * transito a un objeto de dominio.
     *
     * @return TrafficPolice: Objeto de entidad del agente de tránsito.
     * @params TrafficPoliceDTO : Objeto de transferencia de datos del agente de
     * transito.
     */
    TrafficPolice convertDTOtoTrafficPolice(TrafficPoliceDTO trafficPoliceDTO, Highway highway) {
        TrafficPolice trafficPolice = new TrafficPolice();
        trafficPolice.setId(trafficPoliceDTO.getId());
        trafficPolice.setName(trafficPoliceDTO.getName());
        trafficPolice.setIdSecretary(trafficPoliceDTO.getIdSecretary());
        trafficPolice.setYearsExperience(trafficPoliceDTO.getYearsExperience());
        trafficPolice.setHighway(highway);
        return trafficPolice;
    }

    /**
     * Método para convertir un objeto tipo agente de
     * tránsito a un objeto de transferencia de datos.
     *
     * @return TrafficPoliceDTO : Objeto de transferencia de datos de agente de
     * transito.
     * @params TrafficPolice : Objeto tipo agente de
     * transito.
     */
    TrafficPoliceDTO convertTrafficPoliceToDTO(TrafficPolice trafficPolice) {
        TrafficPoliceDTO trafficPoliceDTO = new TrafficPoliceDTO();
        trafficPoliceDTO.setId(trafficPolice.getId());
        trafficPoliceDTO.setName(trafficPolice.getName());
        trafficPoliceDTO.setIdSecretary(trafficPolice.getIdSecretary());
        trafficPoliceDTO.setYearsExperience(trafficPolice.getYearsExperience());
        trafficPoliceDTO.setHighway(trafficPolice.getHighway().getId());
        return trafficPoliceDTO;
    }

    /**
     * Método para convertir un objeto de transferencia de datos de historial a un
     * objeto de dominio.
     *
     * @return historyDTO: Objeto de transferencia de datos del historia.
     * @params History: Objeto tipo historia.
     */
    private HistoryDTO convertHistoryToDTO(History history) {
        HistoryDTO historyDTO = new HistoryDTO();
        historyDTO.setHighwayId(history.getHighwayId());
        historyDTO.setTransitAgentId(history.getTransitAgentId());
        historyDTO.setDate(history.getDate());
        return historyDTO;
    }

}
