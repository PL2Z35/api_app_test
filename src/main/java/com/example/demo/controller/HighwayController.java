package com.example.demo.controller;

import com.example.demo.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.demo.service.HighwayService;
import com.example.demo.service.HistoryService;
import com.example.demo.dto.HighwayDTO;
import com.example.demo.dto.HistoryDTO;
import com.example.demo.entity.Highway;
import com.example.demo.entity.History;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase controladora del objeto Highway.
 *
 * @author : Cristian Plazas
 * @since : 6/27/2022
 */
@RestController
@RequestMapping("/highway")
@CrossOrigin(origins = "http://localhost:81")
public class HighwayController {

    @Autowired
    private HighwayService service;

    @Autowired
    private HistoryService serviceHistory;

    /**
     * Método que devuelve todas las vías.
     * <p>
     * 1. Obtiene todas las vías.
     * 2. Convierte la lista del paso 1 en una lista de objetos tipo HighwayDTO.
     *
     * @return ResponseEntity: Con estado 200 y la lista de vías tipo HighwayDTO.
     */
    @GetMapping("/all")
    public ResponseEntity<List<HighwayDTO>> listHighway() {
        List<Highway> highways = service.allList();
        List<HighwayDTO> highwayDTOs = new ArrayList<>();
        for (Highway highway : highways) {
            highwayDTOs.add(convertDto(highway));
        }
        return new ResponseEntity<>(highwayDTOs, HttpStatus.OK);
    }

    /**
     * Método que devuelve las vías disponibles para ser asignadas.
     * <p>
     * 1. Obtiene todas las vías.
     * 2. Convierte la lista del paso 1 en una lista de objetos tipo HighwayDTO.
     *
     * @return ResponseEntity: Con estado 200 y la lista de vías tipo HighwayDTO.
     */
    @GetMapping("/available")
    public ResponseEntity<List<HighwayDTO>> listHighwayAvailable() {
        List<Highway> highways = service.allAvailable();
        List<HighwayDTO> highwayDTOs = new ArrayList<>();
        for (Highway highway : highways) {
            highwayDTOs.add(convertDto(highway));
        }
        return new ResponseEntity<>(highwayDTOs, HttpStatus.OK);
    }

    /**
     * Método que devuelve una via por su id.
     * <p>
     * 1. Realiza una conversion de objeto de tipo HighwayDTO a objeto de tipo
     * Highway.
     * 2. Ejecuta la búsqueda de la via mediante el objeto del paso 1.
     *
     * @return ResponseEntity: Con estado 200 y la via, si se encuentra la via en la
     * base de datos.
     * @throws NullPointerException: Con estado 404 si no se encuentra la via en la base
     *                        de datos.
     */
    @GetMapping({"/{id}"})
    public ResponseEntity<Object> getHighway(@PathVariable(value = "id") Integer id) {
        try {
            return new ResponseEntity<>(convertDto(service.getHighwayId(id)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Método que devuelve la historia de una via por su id.
     *
     * 1. Realiza una búsqueda de las historias de la via.
     * 2. Convierte la lista del paso 1 en una lista de objetos tipo HistoryDTO.
     *
     * @return ResponseEntity: Con estado 200 y la lista de historias tipo
     * HistoryDTO.
     */
    @GetMapping({"/{id}/history"})
    public ResponseEntity<List<HistoryDTO>> getHighwayHistory(@PathVariable(value = "id") Integer id) {
        List<HistoryDTO> historyDTOs = new ArrayList<>();
        List<History> histories = serviceHistory.getListHighway(id);
        for (History history : histories) {
            historyDTOs.add(convertDtoHistory(history));
        }
        return new ResponseEntity<>(historyDTOs, HttpStatus.OK);
    }

    /**
     * Método que crea una via.
     * <p>
     * 1. Convierte todos los atributos del objeto HighwayDTO a objeto de tipo
     * Highway.
     * 2. Realiza la búsqueda del objeto 1 en la base de datos.
     *
     * @return ResponseEntity: Con estado 200 y se almacena la via correctamente o ResponseEntity: Con estado 409, si se encuentra problemas al
     * almacenar la via.
     */
    @PostMapping()
    public ResponseEntity<HighwayDTO> saveHighway(@RequestBody HighwayDTO highway) {
        if (service.saveHighway(convertDTOtoHighway(highway)) != null) {
            return new ResponseEntity<>(highway, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    /**
     * Método que actualiza una via.
     * <p>
     * 1. Convierte todos los atributos del objeto via tipo HighwayDTO a objeto de
     * tipo Highway.
     * 2. Realiza la búsqueda del objeto 1 en la base de datos.
     *
     * @return ResponseEntity: Con estado 200 y si se encontró la via y se modificó
     * correctamente o ResponseEntity: Con estado 409 si no se encuentra la via en la base
     * de datos.
     */
    @PutMapping()
    public ResponseEntity<HighwayDTO> updateHighway(@RequestBody HighwayDTO highway) {
        if (service.updateHighway(convertDTOtoHighway(highway)) != null) {
            return new ResponseEntity<>(highway, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    /**
     * Método que elimina una via.
     * <p>
     * 1. Convierte todos los atributos del objeto via tipo HighwayDTO a objeto de
     * tipo Highway.
     * 2. Realiza la búsqueda del objeto 1 en la base de datos.
     *
     * @return ResponseEntity: Con estado 200 y si se encuentra el objeto y se
     * elimina correctamente o ResponseEntity: Con estado 404 si no se encuentra la via en la base
     * de datos.
     */
    @DeleteMapping({"/{id}"})
    public ResponseEntity<String> deleteHighway(@PathVariable(value = "id") Integer id) {
        try {
            service.deleteHighway(id);
            return new ResponseEntity<>( HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Convierte un objeto via a un objeto de transferencia de via.
     * <p>
     * Convierte todos los atributos del objeto via en DTO.
     *
     * @param highway: Objeto tipo via.
     * @return HighwayDTO: Objeto de transferencia de datos de via.
     */
    HighwayDTO convertDto(Highway highway) {
        HighwayDTO highwayDto = new HighwayDTO();
        highwayDto.setId(highway.getId());
        highwayDto.setCongestionLevel(highway.getCongestionLevel());
        highwayDto.setNumber(highway.getNumber());
        highwayDto.setStreetOrRace(highway.getStreetOrRace());
        highwayDto.setType(highway.getType());
        return highwayDto;
    }

    /**
     * Convierte un objeto de transferencia de datos de via a un objeto via.
     * <p>
     * Convierte todos los atributos del objeto DTO en atributos para el objeto via.
     *
     * @param highwayDto Objeto de transferencia de datos de via.
     * @return Highway : Objeto via.
     */
    Highway convertDTOtoHighway(HighwayDTO highwayDto) {
        Highway highway = new Highway();
        highway.setId(highwayDto.getId());
        highway.setType(highwayDto.getType());
        highway.setStreetOrRace(highwayDto.getStreetOrRace());
        highway.setNumber(highwayDto.getNumber());
        highway.setCongestionLevel(highwayDto.getCongestionLevel());
        return highway;
    }

    /**
     * Convierte un objeto de historia de via a un objeto de transferencia de
     * historia de via.
     * <p>
     * Convierte todos los atributos del objeto via en DTO.
     *
     * @param history: Objeto de historia de via.
     * @return HistoryDTO : Objeto de transferencia de datos de historia de via.
     */
    HistoryDTO convertDtoHistory(History history) {
        HistoryDTO historyDto = new HistoryDTO();
        historyDto.setDate(history.getDate());
        historyDto.setHighwayId(history.getHighwayId());
        historyDto.setTransitAgentId(history.getTransitAgentId());
        return historyDto;
    }
}
